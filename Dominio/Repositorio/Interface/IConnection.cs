using System.Data;

/// <summary>
/// Utilizado para Conectar ao banco de dados
/// </summary>
namespace pizzaria.Dominio.Repositorio.Interface
{
    public interface IConnection
    {
        
        IDbConnection CriarConexao();
        
    }
}