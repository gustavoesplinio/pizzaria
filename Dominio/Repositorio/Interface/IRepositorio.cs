using System.Collections.Generic;

namespace pizzaria.Dominio.Repositorio.Interface
{
    /// <summary>
    /// Repositório genérico
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IRepositorio<T> where T : new()
    {
         IEnumerable<T> ObterTodos();
         T Obter (int id);
         void Inserir (T obj);
         void Atualizar(T obj);
        void Deletar(int id);
        
    }
}