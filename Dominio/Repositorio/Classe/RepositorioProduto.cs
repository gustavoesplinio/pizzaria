using System.Linq;
using Dapper;
using pizzaria.Dominio.Entidade;
using pizzaria.Dominio.Repositorio.Interface;


/// <summary>
/// Repositório do produto
/// </summary>
namespace pizzaria.Dominio.Repositorio.Classe
{
    public class RepositorioProduto : IRepositorio<Produto>
    {
        IConnection con;
        public RepositorioProduto(IConnection conexao)
        {
            this.con = conexao;
        }
        //Atualizar produto da base de dados
        public void Atualizar(Produto produto)
        {
             using (var conexao = con.CriarConexao())

            conexao.Query<Produto>($@"UPDATE produto SET Id = @id, produto = @Produto, valor = @Valor 
                                    WHERE id = @Id", new 
                                    {
                                    id = produto.Id,
                                    produto = produto.produto,
                                    valor = produto.Valor      
                                    });
        }

        //Deletar produto da base de dados
        public void Deletar(int id)
        {
            using(var conexao = con.CriarConexao())
                  
            conexao.Query<Produto>($@"DELETE FROM produto 
                                    WHERE id = @Id", new
                                    {
                                        Id = id
                                    });
        }

        //Inserir produto na base de dados
        public void Inserir(Produto produto)
        {
            
            using(var conexao = con.CriarConexao())
                {
                    conexao.Query<Produto>($@"INSERT INTO produto VALUES
                                            (@id,@produto,@valor)", new
                                                {
                                                id = produto.Id,
                                                produto = produto.produto,
                                                valor = produto.Valor 
                                                });

                }

        }
        //Obter produto cujo id = @id
        public Produto Obter(int id)
        {
        
        using (var conexao = con.CriarConexao())
        {
            
            return conexao.Query<Produto>($@"SELECT id, produto, valor
                                         FROM produto WHERE id = @Id", new 
                                         {
                                             Id =id
                                            
                                        }).FirstOrDefault();
        }
            
        }


        
        //Obter todos os produtos
        public System.Collections.Generic.IEnumerable<Produto> ObterTodos()
        {

            using(var conexao = con.CriarConexao())
            {
                return conexao.Query<Produto>($@"SELECT *
                                             FROM produto ORDER BY ID");
            }
        }

    }
}