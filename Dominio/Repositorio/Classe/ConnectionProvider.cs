using System.Data;
using Microsoft.Extensions.Configuration;
using pizzaria.Dominio.Repositorio.Interface;
using Npgsql;
namespace pizzaria.Dominio.Repositorio.Classe
{
    public class ConnectionProvider : IConnection
    {
        IConfiguration configurador;
        private string ConnectionString;
       
        public ConnectionProvider(IConfiguration config)
        {
            configurador = config;
        }

        public IDbConnection CriarConexao()
        {
            ConnectionString = configurador["ConnectionStrings:Database"];
            return new NpgsqlConnection(ConnectionString);
        }
    }
}