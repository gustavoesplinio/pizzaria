using System.Linq;
using Dapper;
using pizzaria.Dominio.Entidade;
using pizzaria.Dominio.Repositorio.Interface;

namespace pizzaria.Dominio.Repositorio.Classe
{
    public class RepositorioPedido : IRepositorio<Pedido>
    {
        IConnection con;
        
    public RepositorioPedido(IConnection conexao)
        {
            con = conexao;
            
        }

        public void Atualizar(Pedido pedido)
        {
         using (var conexao = con.CriarConexao())

            conexao.Query<Pedido>($@"UPDATE pedido SET Id = @Id, cliente_id = 
            @Cliente_id, tamanho_id = @Tamanho_id, sabor_id = @Sabor_id,
             sabor2_id = @Sabor2_id, sabor3_id = @Sabor3_id, bebida_id = @Bebida_id WHERE id = @Id", new 
                                {
                                
                                Id = pedido.Id,
                                cliente_id = pedido.Cliente_id,
                                tamanho_id = pedido.Tamanho_id,
                                sabor_id = pedido.Sabor_id,
                                sabor2_id = pedido.Sabor2_id,
                                sabor3_id = pedido.Sabor3_id,
                                bebida_id = pedido.Bebida_id,
                                data = pedido.Data.TimeOfDay,
                                entrega = pedido.Entrega
                                });
        }

        public void Deletar(int id)
        {
            using(var conexao = con.CriarConexao())
                  
            conexao.Query<Pedido>($@"DELETE FROM pedido 
                                    WHERE id = @Id", new
                                    {
                                        Id = id
                                    });
        }

        public void Inserir(Pedido pedido)
        {
           
        using(var conexao = con.CriarConexao())
            {
                conexao.Query<Pedido>($@"INSERT INTO pedido VALUES
                                        (@Id, @Cliente_id, @Tamanho_id, @Sabor_id, @Sabor2_id,
                                         @Sabor3_id, @Bebida_id)", new
                                            {
                                            Id = pedido.Id,
                                            cliente_id = pedido.Cliente_id,
                                            tamanho_id = pedido.Tamanho_id,
                                            sabor_id = pedido.Sabor_id,
                                            sabor2_id = pedido.Sabor2_id,
                                            sabor3_id = pedido.Sabor3_id,
                                            bebida_id = pedido.Bebida_id,
                                            entrega = pedido.Entrega,
                                            data = pedido.Data.TimeOfDay
                                            });


                }
            
        }   

        public Pedido Obter(int id)
        {
        using (var conexao = con.CriarConexao())
        {
            
            return conexao.Query<Pedido>($@"SELECT *
                                         FROM pedido WHERE @id = Id", new 
                                        {
                                             Id =id
                                            
                                        }).FirstOrDefault();
            }
        
        }

        public System.Collections.Generic.IEnumerable<Pedido> ObterTodos()
        {
            using (var conexao = con.CriarConexao())
            {
                return conexao.Query<Pedido>($@"SELECT * 
                                             FROM pedido");
            }
        }
    }
}