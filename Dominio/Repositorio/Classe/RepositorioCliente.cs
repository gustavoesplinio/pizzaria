using pizzaria.Dominio.Entidade;
using pizzaria.Dominio.Repositorio.Interface;
using Dapper;
using System.Linq;

namespace pizzaria.Dominio.Repositorio.Classe
{
    public class RepositorioCliente : IRepositorio<Cliente>
    {
        IConnection con;

        public RepositorioCliente(IConnection conexao)
        {
            con = conexao;
        }

        public void Atualizar(Cliente cliente)
        {
            using (var conexao = con.CriarConexao())

            conexao.Query<Cliente>($@"UPDATE cliente SET nome = @Nome, telefone = @Telefone, endereco = @Endereco 
                                    WHERE id = @Id", new 
                                {
                                    nome = cliente.Nome,
                                    telefone = cliente.Telefone,
                                    endereco = cliente.Endereco
                                });

        }



        public void Deletar(int id)
        {
            using(var conexao = con.CriarConexao())
                  
            conexao.Query<Cliente>($@"DELETE FROM cliente 
                                    WHERE id = @Id", new
                                    {
                                        Id = id
                                    });
        }

        public void Inserir(Cliente cliente)
        {
        
         

         using(var conexao = con.CriarConexao())
        
        
        
         conexao.Query<Cliente>($@"INSERT INTO cliente (nome, telefone, endereco) 
                                VALUES (@Nome, @Telefone, @Endereco)", new 
                                {
                                    nome = cliente.Nome,
                                    telefone = cliente.Telefone,
                                    endereco = cliente.Endereco
                                });
        
        }

        public Cliente Obter(int id)
        {
            
        using (var conexao = con.CriarConexao())
        {
            
            return conexao.Query<Cliente>($@"SELECT *
                                         FROM cliente WHERE id = @Id", new 
                                         {
                                             Id =id
                                            
                                        }).FirstOrDefault();
            }
        }



        public System.Collections.Generic.IEnumerable<Cliente> ObterTodos()
        
        {
         
         
             using(var conexao = con.CriarConexao())
            {
                return conexao.Query<Cliente>($@"SELECT *
                                         FROM cliente");
            }
            
        }
    }
}