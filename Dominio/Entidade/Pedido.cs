using System;




/// <summary>
/// Entidade do pedido, utilizei foreign key dentro do postgres para comunicação entre produto e pedido
/// </summary>
/// 
namespace pizzaria.Dominio.Entidade
{
    public class Pedido
    {
    
        public DateTime Data = DateTime.Now; 
        public int Id { get; set;}
        public int Cliente_id  { get; set;}
        public int Tamanho_id  { get; set;}
        public int Sabor_id  { get; set;}
        public int Sabor2_id  { get; set;}  
        public int Sabor3_id  { get; set;}
        public int Bebida_id { get; set;}
        public string Entrega = ("O pedido será entregue dentro de 1h");        
        
    }
    
} 