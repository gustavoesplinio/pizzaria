namespace pizzaria.Dominio.Entidade
{
    
    public class Cliente
    {
        public int Id {get; set;}
        public string Nome {get; set;}
        public int Telefone {get; set;}
        public string Endereco {get; set;}
    }
}