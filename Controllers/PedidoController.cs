using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using pizzaria.Dominio.Entidade;
using pizzaria.Dominio.Repositorio.Classe;
using pizzaria.Dominio.Repositorio.Interface;


/// <summary>
/// Pedido Controller
/// </summary>

namespace pizzaria.Controllers
{
    //using pizzaria.Models;
    [Route("api/[controller]")]
    [ApiController]
    public class PedidoController : ControllerBase
    {
        IRepositorio<Pedido> repositorio;
        public PedidoController(IConfiguration config)
        {
            var cp = new ConnectionProvider(config);
             repositorio = new RepositorioPedido(cp);          
            
        }

        // GET api/pedido
        //Obter todos os pedidos.

        [HttpGet("")]
        public ActionResult<IEnumerable<Pedido>> Get()
        {
            var result = repositorio.ObterTodos().ToList();
            return result;
        }

        // GET api/pedido/5
        // Obter o pedido por id
        [HttpGet("{id}")]
        public ActionResult<string> GetstringById(int id)
        {
            var result = repositorio.Obter(id);
            if(result == null)
            {
                return NotFound();
            }
            return Ok(result);
        }

        // POST api/pedido
        // Inserir um novo pedido
        [HttpPost("")]
        public ActionResult<IEnumerable<Pedido>> Poststring([FromBody]Pedido pedido)
        {
            try
            {
                repositorio.Inserir(pedido);
            }
            catch (System.Exception)
            {
                
                throw;
            }
            return Ok();
        }

        // PUT api/pedido/5
        //Atualizar um pedido já existente.
        [HttpPut("{id}")]
        public ActionResult<IEnumerable<Pedido>> Putstring(int id,[FromBody] Pedido pedido)
        {
            try
            {
                repositorio.Atualizar(pedido);
            }
            catch (System.Exception)
            {
                
                throw;
            }
            return Ok();
        }

        // DELETE api/pedido/5
        // Deletar pedido.
        [HttpDelete("{id}")]
        public IActionResult DeletestringById(int id)
        {
            try
            {
                repositorio.Deletar(id);
            }
            catch (System.Exception)
            {
                
                throw;
            }
            return Ok();
        }
        
    }
    
}