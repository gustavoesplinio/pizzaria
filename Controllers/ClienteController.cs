
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using pizzaria.Dominio.Entidade;
using pizzaria.Dominio.Repositorio.Classe;
using pizzaria.Dominio.Repositorio.Interface;


namespace pizzaria.Controllers
{
   
   
    [Route("api/[controller]")]
    [ApiController]
    public class ClienteController : ControllerBase
    {
        IRepositorio<Cliente> repositorio;

      
        public ClienteController(IConfiguration config)
        {
            var cp = new ConnectionProvider(config);
            repositorio = new RepositorioCliente(cp);
        }
        // GET api/cliente
        [HttpGet]
        public ActionResult<IEnumerable<Cliente>> Get()
        {
            var result = repositorio.ObterTodos().ToList();
            return result;
        }   
         [HttpGet("{id}")]
        
        public ActionResult<IEnumerable<Cliente>> Get(int id)
        {
        var result = repositorio.Obter(id);
        if (result == null)
        {
            return NotFound();
        }
        return Ok(result);
          
        }
        
        [HttpPost]
        
        public ActionResult<IEnumerable<Cliente>> Post([FromBody]Cliente cliente)
        {
            
             
            try
            {

                 repositorio.Inserir(cliente);
           
            }
            catch (System.Exception)
            {
                
                throw;
            }
            
            return Ok();
            
        }   
        

        [HttpPut("{id}")]
        
        public ActionResult<IEnumerable<Cliente>> Put(int id,[FromBody] Cliente cliente)
        {
            
             try
             {
                 repositorio.Atualizar(cliente);
             }
             catch (System.Exception)
             {
                 
                 throw;
             }
                
            
            return Ok();
        }      
        
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                repositorio.Deletar(id);
            
            }
            catch (System.Exception)
            {
                
                throw;
            }
            return Ok();
        }

    }
}