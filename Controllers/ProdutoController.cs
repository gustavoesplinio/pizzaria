using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using pizzaria.Dominio.Entidade;
using pizzaria.Dominio.Repositorio.Classe;
using pizzaria.Dominio.Repositorio.Interface;

namespace pizzaria.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProdutoController : ControllerBase
    {
     IRepositorio<Produto> repositorio;

      
        public ProdutoController(IConfiguration config)
        {
            var cp = new ConnectionProvider(config);
            repositorio = new RepositorioProduto(cp);
        }
        // GET api/produto
        [HttpGet]

        public ActionResult<IEnumerable<Produto>> Get()
        {
            var result = repositorio.ObterTodos().ToList();
            return result;
        }   
         [HttpGet("{id}")]
        
        public ActionResult<IEnumerable<Produto>> Get(int id)
        {
        var result = repositorio.Obter(id);
        if (result == null)
        {
            return NotFound();
        }
        return Ok(result);
          
        }
        
        [HttpPost]
        
        public ActionResult<IEnumerable<Produto>> Post([FromBody]Produto produto)
        {
            
             
            try
            {

                 repositorio.Inserir(produto);
           
            }
            catch (System.Exception)
            {
                
                throw;
            }
            
            return Ok();
            
        }   
        

        [HttpPut("{id}")]
        
        public ActionResult<IEnumerable<Produto>> Put(int id,[FromBody] Produto produto)
        {
            
             try
             {
                 repositorio.Atualizar(produto);
             }
             catch (System.Exception)
             {
                 
                 throw;
             }
                
            
            return Ok();
        }      
        
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                repositorio.Deletar(id);
            
            }
            catch (System.Exception)
            {
                
                throw;
            }
            return Ok();
        }   
    }
}